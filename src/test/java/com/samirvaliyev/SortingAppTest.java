package com.samirvaliyev;

import static junit.framework.TestCase.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class SortingAppTest {

    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    private final String[] arrayTest;
    private final String expected;

    public SortingAppTest(ImmutablePair<String, String> pair) {
        arrayTest = pair.left.split(" ");
        expected = pair.right;
    }
    @Test(expected = IllegalArgumentException.class)
    public void testZeroArgsCase(){
        String[] zeroArgsArray = new String[0];
        SortingApp.main(zeroArgsArray);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOneArgCase(){
        String[] oneArgArray = new String[1];
        SortingApp.main(oneArgArray);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMoreThanTenArgsCase(){
        String[] zeroArray = new String[11];
        SortingApp.main(zeroArray);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenNotIntegerArgPassed() {
        String[] args = new String[0];
        SortingApp.main(args);
    }
    @Parameters
    public static Object[] checkArrayElements() {
        return new ImmutablePair[]{
                new ImmutablePair<>("1 -2", "-2 1"),
                new ImmutablePair<>("100 41 -5 0", "-5 0 41 100"),
                new ImmutablePair<>("555 45 -1 1 0 10", "-1 0 1 10 45 555"),
                new ImmutablePair<>("9 5 4 1 2 3 7 6 10 8", "1 2 3 4 5 6 7 8 9 10")};
    }

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @Test
    public void whenValidArgsPassed() {
        SortingApp.main(arrayTest);
        assertEquals(expected, extractLastOutputLine(outputStreamCaptor.toString()));
    }

    @After
    public void tearDown() {
        System.setOut(standardOut);
    }

    private String extractLastOutputLine(String output) {
        String[] lines = output.split(System.lineSeparator());
        return lines[lines.length - 1];
    }
}
