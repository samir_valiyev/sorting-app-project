package com.samirvaliyev;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * The SortingApp class is sorts an array of integers in ascending order.
 */
public class SortingApp {


    public static final int MIN_ARGS = 1;
    public static final int MAX_ARGS = 10;
    private static final Logger LOG = LogManager.getLogger(SortingApp.class);

    public static void main(String[] args) {
        /*
         * @param args the command-line arguments.
         * @param numbers the array to be sorted.
         * @return a string representation of the sorted array.
         * @throws IllegalArgumentException if the array length is 0, 1 and more than 10.
         */

        LOG.info("Sorting App Project Starts");

        if (!(args.length >= MIN_ARGS && args.length <= MAX_ARGS)) {
            LOG.error("You have entered an incorrect array element");
            throw new IllegalArgumentException(String.format("You must enter array element between %d and %d", MIN_ARGS, MAX_ARGS));
        }

        int[] numbers = changeToIntArray(args);
        LOG.info("Sorting the array elements in ascending order");
        Arrays.sort(numbers);
        System.out.println(addSpaceBetweenNumbers(numbers));

        LOG.info("Sorting App Project Ends");

    }

    private static int[] changeToIntArray(String[] stringElements) {

        int[] numbers = new int[stringElements.length];

        for (int i = 0; i < stringElements.length; i++) {
            if (NumberUtils.isParsable(stringElements[i])) {
                numbers[i] = Integer.parseInt(stringElements[i]);

            } else {
                LOG.error("Please enter correct array element");
                throw new IllegalArgumentException("'" + stringElements[i] + "' is not parsable an integer");
            }
        }
        return numbers;
    }

    private static String addSpaceBetweenNumbers(int[] numbers) {
        return Arrays.stream(numbers).mapToObj(String::valueOf)
                .collect(Collectors.joining(" "));
    }
}
